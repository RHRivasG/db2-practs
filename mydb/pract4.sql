SET SERVEROUTPUT ON;

CREATE TABLE log_creates(
  user_id NUMBER,
  table_name VARCHAR(100),
  creation_date DATE,
  CONSTRAINT log_pk PRIMARY KEY (user_id, creation_date)
);

CREATE OR REPLACE TRIGGER log_creations
AFTER CREATE ON DATABASE
BEGIN
    IF ORA_DICT_OBJ_TYPE = 'TABLE' THEN
        INSERT INTO log_creates(user_id, table_name, creation_date)
        VALUES (UID, ORA_DICT_OBJ_NAME, SYSDATE);
    END IF;
END;
/

CREATE TABLE ejemplo(
  id NUMBER,
  nombre VARCHAR(20)
);

SELECT * FROM log_creates;
