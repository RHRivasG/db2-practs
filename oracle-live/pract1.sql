CREATE OR REPLACE TYPE HISTORICO AS OBJECT(
  fecha_inicial DATE,
  fecha_final DATE,
  STATIC PROCEDURE final_validate (fecha_inicial DATE,fecha_final DATE),
  MEMBER FUNCTION is_validate RETURN HISTORICO
);

/

CREATE OR REPLACE TYPE BODY HISTORICO AS
  STATIC PROCEDURE final_validate (fecha_inicial date, fecha_final date)
  IS
  BEGIN
    IF ( NOT ( fecha_final is null or fecha_final - fecha_inicial > 0) ) THEN
      RAISE_APPLICATION_ERROR(-20001,'Fecha final invalida');
    END IF;
  END;

  MEMBER FUNCTION is_validate RETURN HISTORICO
  IS
  BEGIN
    historico.final_validate(fecha_inicial,fecha_final);
    RETURN SELF;
  END;
END;

/

CREATE TABLE vacunas_informacion(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  fase NUMBER,
  estado NUMBER(1),
  hist HISTORICO NOT NULL
);

CREATE TABLE jornadas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  dosis NUMBER NOT NULL,
  hist HISTORICO NOT NULL
);

INSERT INTO jornadas(dosis, hist) VALUES (
  1,
  HISTORICO(DATE '2021-05-14', DATE '2021-05-16').is_validate()
);

INSERT INTO jornadas(dosis, hist) VALUES (
  1,
  HISTORICO(DATE '2021-05-14', NULL).is_validate()
);

SELECT j.id, j.dosis, j.hist.fecha_inicial, j.hist.fecha_final from jornadas j;

--Insert que debería fallar, todo a partir de aquí no debería ejecutarse
INSERT INTO jornadas(dosis, hist) VALUES (
  1,
  HISTORICO(DATE '2021-05-14', DATE '2021-05-13').is_validate()
);
COMMIT;

SELECT j.id, j.dosis, j.hist.fecha_inicial, j.hist.fecha_final from jornadas j;
