-- Crear TDA's --

CREATE OR REPLACE TYPE HISTORICO AS OBJECT(
  fecha_inicial DATE,
    fecha_final DATE,
    STATIC PROCEDURE final_valido (fecha_inicial DATE,fecha_final DATE),
    MEMBER FUNCTION es_valido RETURN HISTORICO,
    MEMBER FUNCTION activo RETURN NUMBER
);

/

CREATE OR REPLACE TYPE BODY HISTORICO AS
  STATIC PROCEDURE final_valido (fecha_inicial date, fecha_final date)
  IS
  BEGIN
    IF ( NOT ( fecha_final is null or fecha_final - fecha_inicial > 0) ) THEN
      RAISE_APPLICATION_ERROR(-20001,'Fecha final invalida');
    END IF;
  END;

  MEMBER FUNCTION es_valido RETURN HISTORICO
  IS
  BEGIN
    historico.final_valido(fecha_inicial,fecha_final);
    RETURN SELF;
  END;

  MEMBER FUNCTION activo RETURN NUMBER
  IS
  BEGIN
    IF (fecha_final is null) THEN
    RETURN 1;
  END IF;
    RETURN 0;
  END;
END;
/

-- Crear dominios --

CREATE TABLE poblacion(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  cantidad NUMBER NOT NULL,
  CONSTRAINT poblacion_pk PRIMARY KEY (id)
);


CREATE TABLE jornadas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  dosis NUMBER NOT NULL,
  hist HISTORICO NOT NULL,
  CONSTRAINT jornada_pk PRIMARY KEY (id)
);

CREATE TABLE informacion_poblacion(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  id_poblacion NUMBER NOT NULL,
  id_jornada NUMBER,
  estado VARCHAR2(50),
  hist HISTORICO NOT NULL,
  CONSTRAINT info_poblacion_pk PRIMARY KEY (id),
  CONSTRAINT fk_info_poblacion
    FOREIGN KEY (id_poblacion)
    REFERENCES poblacion(id),
  CONSTRAINT fk_info_jornada
    FOREIGN KEY (id_jornada)
    REFERENCES jornadas(id)
);

CREATE TABLE efectos_secundarios(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  nombre NUMBER NOT NULL,
  CONSTRAINT efecto_sec_pk PRIMARY KEY (id)
);

CREATE TABLE efectos_observados(
  id_efect_sec NUMBER,
  id_info_pob NUMBER,
  CONSTRAINT efecto_ob_pk PRIMARY KEY (id_efect_sec, id_info_pob)
);

-- Modificar ciertas tablas --

ALTER TABLE informacion_poblacion
ADD (cantidad NUMBER NOT NULL);

ALTER TABLE efectos_observados
ADD (cantidad NUMBER NOT NULL);

ALTER TABLE efectos_secundarios
MODIFY nombre VARCHAR2(20);

-- Seleccion de poblacion vacunada, trigger after --

CREATE OR REPLACE TRIGGER vacunar_en_jornada
       AFTER INSERT ON jornadas
       FOR EACH ROW
DECLARE
  id_pob poblacion.id%type;
  jornada_id jornadas.id%type;
  cant_pob poblacion.cantidad%type;
  vac_cant_p informacion_poblacion.cantidad%type;
  semilla NUMBER(20);
BEGIN
  -- Inicializar la semilla de numeros aleaorios --
  semilla := TO_NUMBER(TO_CHAR(SYSDATE,'YYYYDDMMSS'));
  dbms_random.initialize(semilla);
  -- Obtener la cantidad de personas en total --
  SELECT p.id, (p.cantidad - SUM(COALESCE(pv.cantidad, 0)))
  INTO id_pob, cant_pob
  FROM poblacion p
  LEFT OUTER JOIN (
       SELECT *
       FROM informacion_poblacion
       WHERE id_jornada IS NOT NULL
    ) pv
  ON pv.id_poblacion = p.id
  GROUP BY p.id, p.cantidad;
  -- Obtener la cantidad de personas a vacunar --
  SELECT 1+ABS(MOD(DBMS_RANDOM.RANDOM, cant_pob)) INTO vac_cant_p FROM dual;
  -- Insertar la poblacion a ser vacunada --
  INSERT INTO informacion_poblacion(id_poblacion, id_jornada, hist, cantidad)
  VALUES (id_pob, :new.id, HISTORICO(CURRENT_DATE, NULL), vac_cant_p);
END;
/

-- Seleccion de poblacion con efectos secundarios, trigger after --

CREATE OR REPLACE TRIGGER efectos_en_vacunacion
       AFTER INSERT ON informacion_poblacion
       FOR EACH ROW
DECLARE
  cant_efs_pob efectos_observados.cantidad%type;
  efs_id efectos_secundarios.id%type;
  semilla NUMBER(20);
BEGIN
  -- Ejecutar solo si la poblacion fue vacunada --
  IF :new.id_jornada IS NOT NULL THEN
     -- Inicializar valores random --
     semilla := TO_NUMBER(TO_CHAR(SYSDATE,'YYYYDDMMSS'));
     dbms_random.initialize(semilla);
     -- Seleccion de efecto secundario registrados --
     SELECT id
     INTO efs_id
     FROM efectos_secundarios
     ORDER BY DBMS_RANDOM.RANDOM
     FETCH FIRST 1 ROWS ONLY;
     -- Cantidad de afectados por el efecto secundario --
     SELECT 1+ABS(MOD(DBMS_RANDOM.RANDOM, :new.cantidad))
     INTO cant_efs_pob
     FROM dual;
     -- Insertar observacion de efecto secundario --
     INSERT INTO efectos_observados(id_efect_sec, id_info_pob, cantidad)
     VALUES (efs_id, :new.id, cant_efs_pob);
  END IF;
END;
/

-- Insertar datos base --

INSERT INTO poblacion (cantidad) VALUES (30000000);

INSERT INTO efectos_secundarios (nombre) VALUES ('Dolor de cabeza');
INSERT INTO efectos_secundarios (nombre) VALUES ('Vomitos');
INSERT INTO efectos_secundarios (nombre) VALUES ('Nauseas');
