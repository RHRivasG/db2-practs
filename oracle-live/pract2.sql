CREATE OR REPLACE TYPE HISTORICO AS OBJECT(
  fecha_inicial DATE,
  fecha_final DATE,
  STATIC PROCEDURE final_valido (fecha_inicial DATE,fecha_final DATE),
  MEMBER FUNCTION es_valido RETURN HISTORICO,
  MEMBER FUNCTION activo RETURN NUMBER
);

/

CREATE OR REPLACE TYPE BODY HISTORICO AS
  STATIC PROCEDURE final_valido (fecha_inicial date, fecha_final date)
  IS
  BEGIN
    IF ( NOT ( fecha_final is null or fecha_final - fecha_inicial > 0) ) THEN
      RAISE_APPLICATION_ERROR(-20001,'Fecha final invalida');
    END IF;
  END;

  MEMBER FUNCTION es_valido RETURN HISTORICO
  IS
  BEGIN
    historico.final_valido(fecha_inicial,fecha_final);
    RETURN SELF;
  END;

  MEMBER FUNCTION activo RETURN NUMBER
  IS
  BEGIN
    IF (fecha_final is null) THEN
	  RETURN 1;
	END IF;
    RETURN 0;
  END;
END;
/

CREATE TABLE personas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  nombre VARCHAR2(25) NOT NULL,
  apellido VARCHAR2(25) NOT NULL,
  cedula NUMBER NOT NULL,
  edad NUMBER NOT NULL,

  CONSTRAINT personas_pk PRIMARY KEY (id)
);

CREATE TABLE tipos_vacuna(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  nombre VARCHAR2(50) NOT NULL,

  CONSTRAINT tipos_vacuna_pk PRIMARY KEY (id)
);

CREATE TABLE lotes(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  id_vacuna NUMBER NOT NULL,
  cantidad NUMBER NOT NULL,

  CONSTRAINT lotes_pk PRIMARY KEY (id),

  CONSTRAINT fk_lotes_vacuna
    FOREIGN KEY (id_vacuna)
    REFERENCES tipos_vacuna(id)
);

CREATE TABLE stock(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  id_lote NUMBER NOT NULL,
  cantidad NUMBER,

  CONSTRAINT stock_pk PRIMARY KEY (id),

  CONSTRAINT fk_stock_lotes
    FOREIGN KEY (id_lote)
    REFERENCES lotes(id)
);

CREATE TABLE jornadas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  dosis NUMBER NOT NULL,
  hist HISTORICO NOT NULL,

  CONSTRAINT jornadas_pk PRIMARY KEY (id)
);

CREATE TABLE insumos_uso(
  id_stock NUMBER,
  id_jornada NUMBER,

  CONSTRAINT insumos_uso_pk PRIMARY KEY (id_stock, id_jornada),

  CONSTRAINT fk_insumo_stock
    FOREIGN KEY (id_stock)
    REFERENCES stock(id),

  CONSTRAINT fk_insumo_jornada
    FOREIGN KEY (id_jornada)
    REFERENCES jornadas(id)
);

CREATE TABLE informacion_personas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL, 
  id_persona NUMBER NOT NULL,
  id_jornada NUMBER,
  estado VARCHAR2(50),
  hist HISTORICO NOT NULL,

  CONSTRAINT info_personas_pk PRIMARY KEY (id),

  CONSTRAINT fk_info_persona
    FOREIGN KEY (id_persona)
    REFERENCES personas(id),

  CONSTRAINT fk_info_jornada
    FOREIGN KEY (id_jornada)
    REFERENCES jornadas(id)
);

CREATE OR REPLACE PROCEDURE primera_dosis
AS
	CURSOR pd_vac IS
      SELECT 
		tv.nombre tipo_vacuna,
		COUNT(*) total
      FROM personas p
      INNER JOIN informacion_personas ip ON p.id = ip.id_persona
      INNER JOIN jornadas j ON ip.id_jornada = j.id
      INNER JOIN insumos_uso iu ON iu.id_jornada = j.id
      INNER JOIN stock s ON s.id = iu.id_stock
      INNER JOIN lotes l ON l.id = s.id_lote
      INNER JOIN tipos_vacuna tv ON tv.id = l.id_vacuna
      WHERE j.dosis = 1
	  GROUP BY tv.nombre;
BEGIN
   FOR pd_vac_rec 
   IN pd_vac
   LOOP
    DBMS_OUTPUT.put_line('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    DBMS_OUTPUT.put_line('Tipo de vacuna:' || pd_vac_rec.tipo_vacuna);
    DBMS_OUTPUT.put_line('Cantidad de personas: ' || pd_vac_rec.total);
	FOR pd_rec 
	IN (
	  SELECT 
		p.nombre, 
		p.apellido, 
		p.cedula, 
		p.edad
      FROM personas p
      INNER JOIN informacion_personas ip ON p.id = ip.id_persona
      INNER JOIN jornadas j ON ip.id_jornada = j.id
      INNER JOIN insumos_uso iu ON iu.id_jornada = j.id
      INNER JOIN stock s ON s.id = iu.id_stock
      INNER JOIN lotes l ON l.id = s.id_lote
      INNER JOIN tipos_vacuna tv ON tv.id = l.id_vacuna
      WHERE j.dosis = 1
      AND tv.nombre = pd_vac_rec.tipo_vacuna)
	LOOP
	 		DBMS_OUTPUT.put_line('---------------------------------------');
      		DBMS_OUTPUT.put_line('Nombre de la persona: ' || pd_rec.nombre || ' ' || pd_rec.apellido); 
      		DBMS_OUTPUT.put_line('Cédula: ' || pd_rec.cedula);
      		DBMS_OUTPUT.put_line('Edad: ' || pd_rec.edad);
	END LOOP;
   END LOOP;
END;
/

insert into personas (nombre, apellido, cedula, edad) values ('Kayla', 'McConnachie', 17729662, 49);
insert into personas (nombre, apellido, cedula, edad) values ('Troy', 'Byram', 15668747, 76);
insert into personas (nombre, apellido, cedula, edad) values ('Jandy', 'Westmacott', 17036439, 6);
insert into personas (nombre, apellido, cedula, edad) values ('Jemimah', 'Batson', 25825555, 58);
insert into personas (nombre, apellido, cedula, edad) values ('Kimberly', 'Daulton', 16593472, 64);
COMMIT;

insert into tipos_vacuna (nombre) values ('Pfizer');
insert into tipos_vacuna (nombre) values ('AstraZeneca');
insert into tipos_vacuna (nombre) values ('Sputnik V');
insert into tipos_vacuna (nombre) values ('Sinopharm');
COMMIT;

insert into lotes (id_vacuna, cantidad) values (1, 2);
insert into lotes (id_vacuna, cantidad) values (2, 1);
insert into lotes (id_vacuna, cantidad) values (3, 1);
COMMIT;

insert into stock (id_lote) values (1);
insert into stock (id_lote) values (2);
insert into stock (id_lote) values (3);
COMMIT;

insert into jornadas(dosis, hist) values (1, HISTORICO(DATE '2021-05-14',DATE '2021-05-15').es_valido());
insert into jornadas(dosis, hist) values (1, HISTORICO(DATE '2021-05-16',DATE '2021-05-17').es_valido());
COMMIT;

insert into insumos_uso(id_stock, id_jornada) values (1, 1);
insert into insumos_uso(id_stock, id_jornada) values (2, 2);
COMMIT;

insert into informacion_personas (id_persona, id_jornada, hist) values (1,1,HISTORICO(DATE '2021-05-14',DATE '2021-05-15'));
insert into informacion_personas (id_persona, id_jornada, hist) values (2,1,HISTORICO(DATE '2021-05-14',DATE '2021-05-15'));
insert into informacion_personas (id_persona, id_jornada, hist) values (3,2,HISTORICO(DATE '2021-05-16',DATE '2021-05-17'));
COMMIT;

EXECUTE primera_dosis;
