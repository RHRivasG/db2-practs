--Tabla de entrada
CREATE TABLE vacunas_usadas(
  id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL,
  fecha DATE NOT NULL,
  dosis_ap NUMBER NOT NULL,
  dosis_dis NUMBER,
  CONSTRAINT vacuna_usada_pk PRIMARY KEY (id)
);

INSERT INTO vacunas_usadas (fecha, dosis_ap, dosis_dis)
VALUES (DATE '2020-06-02', 100, NULL);

INSERT INTO vacunas_usadas (fecha, dosis_ap, dosis_dis)
VALUES (DATE '2020-06-08', 500, NULL);

INSERT INTO vacunas_usadas (fecha, dosis_ap, dosis_dis)
VALUES (DATE '2020-06-03', 200, NULL);

INSERT INTO vacunas_usadas (fecha, dosis_ap, dosis_dis)
VALUES (DATE '2020-06-04', 300, NULL);

INSERT INTO vacunas_usadas (fecha, dosis_ap, dosis_dis)
VALUES (DATE '2020-06-01', 2000, NULL);

COMMIT;

--Dosis disponibles por dia:
DECLARE
  vacunas_total NUMBER;
  CURSOR vu_cursor IS
	SELECT * FROM vacunas_usadas
	ORDER BY fecha;
BEGIN
  vacunas_total := 4000;
  FOR vu IN vu_cursor LOOP
	vacunas_total := vacunas_total-vu.dosis_ap;
	UPDATE vacunas_usadas 
	SET dosis_dis = vacunas_total
	WHERE id = vu.id;
  END LOOP;
END;
/

--Tabla resultante:
SELECT * FROM vacunas_usadas ORDER BY fecha;
DROP TABLE vacunas_usadas;
